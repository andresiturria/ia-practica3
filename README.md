# Practica 3 #
![CC](Imagenes/LicenciaCC.png)


# Problema #

Predecir el ganador del premio Cy Young (premio al mejor pitcher en la MLB), este premio se otorga en base a una votación al final de la temporada, por lo cual es interesante predecir el ganador.  



# Fuente de los datos #
BBDD de Baseball extraida de http://www.seanlahman.com/baseball-archive/statistics/
Se extrajeron los registros desde 1980 hasta 2017 de la tabla pitchers y se añadió la columna Cy Young, con valor true si ese pitcher lo gano en ese año, el fichero resultante se puede encontrar en la carpeta doc.


# Identificar las características relevantes de los hechos #
En este caso todas las estadísticas son relevantes a excepción de los campos playerID, yearID, stint, teamID y lgID, que sólo proporcionan datos para identificar al jugador, por lo cual creamos otro csv
sin estas columnas para generar el .arff que utilizaremos para entrenar el modelo.

# Evaluación de modelos #
### J48 ###

=== Summary ===

Correctly Classified Instances       22729               99.6012 %  
Incorrectly Classified Instances        91                0.3988 %  
Kappa statistic                          0.3138  
Mean absolute error                      0.0048  
Root mean squared error                  0.0606  
Relative absolute error                 73.1113 %  
Root relative squared error            106.5725 %  
Total Number of Instances            22820       

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.998    0.716    0.998      0.998    0.998      0.316    0.672     0.997     FALSE
                 0.284    0.002    0.356      0.284    0.316      0.316    0.672     0.169     TRUE
Weighted Avg.    0.996    0.714    0.996      0.996    0.996      0.316    0.672     0.994

### Random Forest ###
=== Summary ===

Correctly Classified Instances       22753               99.7064 %  
Incorrectly Classified Instances        67                0.2936 %  
Kappa statistic                          0.2786  
Mean absolute error                      0.0046  
Root mean squared error                  0.0473  
Relative absolute error                 70.7053 %  
Root relative squared error             83.2566 %  
Total Number of Instances            22820     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 1.000    0.824    0.997      1.000    0.999      0.346    0.967     1.000     FALSE
                 0.176    0.000    0.684      0.176    0.280      0.346    0.967     0.451     TRUE
Weighted Avg.    0.997    0.822    0.996      0.997    0.996      0.346    0.967     0.998     

### Multilayer Perceptron ###
=== Summary ===

Correctly Classified Instances       22747               99.6801 %  
Incorrectly Classified Instances        73                0.3199 %  
Kappa statistic                          0.4325  
Mean absolute error                      0.0033  
Root mean squared error                  0.0531  
Relative absolute error                 50.1555 %  
Root relative squared error             93.3728 %  
Total Number of Instances            22820     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.999    0.622    0.998      0.999    0.998      0.437    0.983     1.000     FALSE
                 0.378    0.001    0.509      0.378    0.434      0.437    0.981     0.417     TRUE
Weighted Avg.    0.997    0.620    0.996      0.997    0.997      0.437    0.983     0.998    

### Naive Bayes ###
=== Summary ===

Correctly Classified Instances       19952               87.4321 %  
Incorrectly Classified Instances      2868               12.5679 %  
Kappa statistic                          0.0386  
Mean absolute error                      0.1254  
Root mean squared error                  0.3494  
Relative absolute error               1925.3803 %  
Root relative squared error            614.5942 %  
Total Number of Instances            22820     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.874    0.095    1.000      0.874    0.933      0.133    0.952     1.000     FALSE
                 0.905    0.126    0.023      0.905    0.045      0.133    0.960     0.081     TRUE
Weighted Avg.    0.874    0.095    0.996      0.874    0.930      0.133    0.952     0.997     


### Decisión ###
Se descarta Naive Bayes de primero por tener un rendimiento menor a los otros algoritmos.  
Los primeros 3 modelos probados tienen un rendimiento muy similar, se selecciona Random Forest por ser el que proporciona mayor porcentaje de Instancias clasificadas correctamente y por ende mayor precisión.


### Aplicación ###
Para generar el ejecutable se puede usar el makefile proporcionado con el target jar.  
Una vez generado el jar, ejecutarlo con el comando java -jar aprendizaje.jar  
La aplicación pide como input el nombre del fichero de datos de prueba a usar, estos ficheros están en la carpeta test_data, se tiene que usar el nombre completo (test_data_false.arff o test_data_true.arff) (nota: usar solo el nombre del fichero, no la ruta completa).



